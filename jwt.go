package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"

	//"net/http"

	_ "github.com/go-sql-driver/mysql"
	//"github.com/gorilla/mux"
)

var mySigningKey = []byte("captainjacksparrowsayshi")

//GenerateToken func
func GenerateToken(w http.ResponseWriter, r *http.Request) {

	type Address struct {
		IP string `json:"ip"`
	}
	var address Address

	_ = json.NewDecoder(r.Body).Decode(&address)
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["ip"] = address.IP
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		err = fmt.Errorf("Something Went Wrong: %s", err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
	}

	doc := map[string]interface{}{

		"token": tokenString,
	}
	respondJSON(w, http.StatusCreated, doc)
}

//CreateTable func
func CreateTable(w http.ResponseWriter, r *http.Request) {

	type Check struct {
		Table string `json:"table"`
	}
	var check Check

	_ = json.NewDecoder(r.Body).Decode(&check)
	// fmt.Fprintf(w, "Hello World")
	// fmt.Println("Endpoint Hit: homePage")
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]

	claims, _ := extractClaims(reqToken)
	ip := claims["ip"]

	fmt.Println("Go MySQL Tutorial")

	// Open up our database connection.
	// I've set up a database on my local machine using phpmyadmin.
	// The database is called testDb

	text := "root:helloworld@tcp(" + ip.(string) + ")/testapp"

	db, err := sql.Open("mysql", text)

	// if there is an error opening the connection, handle it
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	// defer the close till after the main function has finished
	// executing
	defer db.Close()

	prepare := "CREATE Table " + check.Table + "(id int NOT NULL AUTO_INCREMENT, name varchar(50), PRIMARY KEY (id));"

	stmt, err := db.Prepare(prepare)
	if err != nil {
		fmt.Println(err.Error())
	}
	_, err = stmt.Exec()
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	text = check.Table + " was created at mysql instance running at ip "+ip.(string)
	respondJSON(w, http.StatusCreated, map[string]string{"success": text})
	return

}

//extractClaims extracts claims made in token
func extractClaims(tokenStr string) (jwt.MapClaims, bool) {
	hmacSecretString := "captainjacksparrowsayshi"
	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	}

	log.Printf("Invalid JWT Token")
	return nil, false

}

//GenerateJWT generates JWT token
func GenerateJWT() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["client"] = "Elliot Forbes"
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		err = fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

// respondJSON makes the response with payload as json format
func respondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(w http.ResponseWriter, code int, message string) {
	respondJSON(w, code, map[string]string{"error": message})
}

//ToJSONCompatibleMap function
func ToJSONCompatibleMap(obj interface{}) (map[string]interface{}, error) {
	objAsBytes, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	var objAsMap map[string]interface{}
	err = json.Unmarshal(objAsBytes, &objAsMap)
	if err != nil {
		return nil, err
	}
	return objAsMap, nil
}
