module gitlab.com/kailehok/test

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
)
