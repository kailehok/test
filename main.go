package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	//"net/http"
	//"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/token", GenerateToken).Methods("POST")
	router.HandleFunc("/create-table", CreateTable).Methods("POST")

	err := http.ListenAndServe(":9000", router)
	if err != nil {
		fmt.Println("inside")
		panic(err)
	}

}
